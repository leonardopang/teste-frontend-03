import { tns } from "../../../node_modules/tiny-slider/src/tiny-slider";

export default () => {
  const valid = document.querySelector('.carrousel-grid-desktop');
  if (valid) {
    const heroHeader = tns({
      container: '.carrousel-menu',
      items: 2,
      autoplay: false,
      controls: false,
      nav: false,
      mouseDrag: true,
      gutter: 16,
      loop: true,
      axis: "vertical",
    });

    /* Control */
    const arrowUp = document.querySelector('.control--up')
    arrowUp.addEventListener('click', () => heroHeader.goTo('prev'))
    const arrowDown = document.querySelector('.control--down')
    arrowDown.addEventListener('click', () => heroHeader.goTo('next'))
  }
}