import { tns } from "../../../node_modules/tiny-slider/src/tiny-slider";

export default () => {
  const valid = document.querySelector('.carrousel-grid-mobile');
  if (valid) {
    const carrousel_mobile = tns({
      container: '.carrousel-menu-mobile',
      items: 1,
      autoplay: false,
      controls: false,
      nav: false,
      mouseDrag: true,
      gutter: 16,
      loop: true,
      responsive: {
        600: {
          items: 2,
        }
      }
    });

    /*Control*/
    const arrowRight = document.querySelector('.control--right')
    arrowRight.addEventListener('click', () => carrousel_mobile.goTo('next'))
  }
}  