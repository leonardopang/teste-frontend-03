'use strict';
import mainStyle from '../css/sass/main.scss';

import slider from './modules/slider';
import sliderMobile from './modules/sliderMobile';

sliderMobile();

slider();