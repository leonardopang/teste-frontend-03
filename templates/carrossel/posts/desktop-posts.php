<?php

$data = get_api('https://teste-frontend.seox.com.br/wp-json/wp/v2/posts?_embed'); //Pegando dados dos Posts


foreach ($data as $index => $post):
  $title = $post->title->rendered;
  $content = $post->content->rendered;
  $excerpt = strip_tags($content); //Removendo Tags HTML
  $excerpt = substr($excerpt, 0, 64) . '...'; //Reduzindo quantidade de caracteres
  $post_url = $post->link;
  $thumbnail_url = '';
  if (isset($post->_embedded->{'wp:featuredmedia'}[0]->source_url)) {
    $thumbnail_url = $post->_embedded->{'wp:featuredmedia'}[0]->source_url;
  }
  $imagem = $thumbnail_url ? $thumbnail_url : site_url() . "/wp-content/uploads/Rectangle-524.png";
  $post_id = $post->id;
  ?>
  <!-- Feature -->
  <?php if ($index == 0): ?>
    <div class="carrousel-feature">
      <?php card_item($title, $imagem, $post_url, $excerpt); ?>
    </div>
    <!-- End Feature -->
    <!-- Carousel Posts -->
    <div class="carrousel-slider">
      <div class="carrousel-menu">
      <?php else: ?>
        <?php card_item($title, $imagem, $post_url, $excerpt); ?>
      <?php endif; ?>
      <?php
endforeach;
?>
    <!-- End Carousel Posts -->
  </div>
</div>