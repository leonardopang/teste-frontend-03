<header class="header-carousel">
  <h2 class="title-header">Vídeos</h2>
  <div class="header-right">
    <div class="button-container">
      <a href="">Veja Mais
        <?php get_svg('arrow-right') ?>
      </a>
    </div>

    <div class="controls">
      <span class="controls-horizontal">
        <span class="control--left">
          <?php get_svg('arrow-right') ?>
        </span>
        <span class="control--right">
          <?php get_svg('arrow-right') ?>
        </span>
      </span>
      <span class="controls-vertical">
        <span class="control--up">
          <?php get_svg('arrow-up') ?>
        </span>
        <span class="control--down">
          <?php get_svg('arrow-down') ?>
        </span>
      </span>
    </div>
  </div>
</header>