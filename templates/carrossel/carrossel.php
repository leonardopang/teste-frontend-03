<section class="carrousel">
  <div class="container">
    <div class="carrousel-container">
      <?php get_file('carrossel/header_carrossel.php'); //Header Carrossel ?>
      <div class="carrousel-grid carrousel-grid-desktop">
        <?php get_file('carrossel/posts/desktop-posts.php'); //Posts Desk?>
      </div>
      <div class="carrousel-grid carrousel-grid-mobile">
        <?php get_file('carrossel/posts/mobile-posts.php'); //Posts Mobile?>
      </div>
    </div>
</section>