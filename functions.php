<?php

define('PATHS_SVG', TEMPLATEPATH . '/assets/images/svg');
define('PATHS_FILE', TEMPLATEPATH . '/templates');

$root_template = get_template_directory();

function reset_header()
{
  remove_action('wp_head', 'rsd_link');
  remove_action('wp_head', 'wlwmanifest_link');
  remove_action('wp_head', 'start_post_rel_link', 10, 0);
  remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
  remove_action('wp_head', 'feed_links_extra', 3);
  remove_action('wp_head', 'wp_generator');
  remove_action('wp_head', 'print_emoji_detection_script', 7);
  remove_action('admin_print_scripts', 'print_emoji_detection_script');
  remove_action('wp_print_styles', 'print_emoji_styles');
  remove_action('admin_print_styles', 'print_emoji_styles');
}
add_action('after_setup_theme', 'reset_header');

function enqueue_styles()
{

  wp_enqueue_style('main', get_stylesheet_directory_uri() . '/public/css/main.css');
}
add_action('wp_enqueue_scripts', 'enqueue_styles', 10);
function sa_sanitize_spanish_chars($filename)
{
  $filename = preg_replace('/[ÍÌÏÎíìîï]/i', 'i', $filename);
  $filename = preg_replace('/[àáãâäÀÁÃÂÄ]/i', 'a', $filename);
  $filename = preg_replace('/[ÉÈÊËéèêë]/i', 'e', $filename);
  $filename = preg_replace('/[úùûüÚÙÛÜ]/i', 'u', $filename);
  $filename = preg_replace('/[ÓÒÖÔÕõôöóò]/i', 'o', $filename);
  $filename = preg_replace('/[ñÑ]/i', 'n', $filename);
  $filename = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $filename);
  $filename = preg_replace('/aa/i', 'a', $filename);
  $filename = preg_replace('/ea/i', 'e', $filename);
  $filename = preg_replace('/ua/i', 'u', $filename);
  $filename = preg_replace('/ia/i', 'i', $filename);
  $filename = preg_replace('/oa/i', 'o', $filename);
  return $filename;
}

add_filter('sanitize_file_name', 'sa_sanitize_spanish_chars', 10);

function get_svg($name) //Puxar svg
{
  require(PATHS_SVG . '/' . $name . '.svg');
}

function get_file($name) //Puxar arquivos
{
  require_once(PATHS_FILE . '/' . $name);
}

function cc_mime_types($mimes)
{
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

add_filter('wpcf7_autop_or_not', '__return_false'); //Remover criação de tag <p>

function pegar_comentarios($post_id)
{
  $url = 'https://teste-frontend.seox.com.br/wp-json/wp/v2/posts/' . $post_id;
  $response = wp_remote_get($url);

  if (is_wp_error($response)) {
    return 0;
  }

  $data = wp_remote_retrieve_body($response);
  $post = json_decode($data);

  if (property_exists($post, 'comment_count')) {
    $comment_count = $post->comment_count;
    if ($comment_count > 0) {
      ?>
      <div class="num-comments">
        <?= get_svg('icon-comments') ?>
        <?= $comment_count ?>
      </div>
      <?php
    }
  }
}

function get_api($url)
{
  $url = $url;
  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
  curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

  $response = curl_exec($ch);
  $data = json_decode($response);

  /* Verificando se contem algum erro */
  if ($data === null && json_last_error() !== JSON_ERROR_NONE) {
    echo 'Erro ao decodificar JSON: ' . json_last_error_msg();
  }

  return $data;
  curl_close($ch);
}

function card_item($title, $imagem, $post_url, $excerpt)
{ ?>
  <div class="carrousel-item">
    <div class="image-container">
      <img src="<?= $imagem ?>" alt="<?= $title ?>">
      <a href="<?= $post_url ?>" class="arrow-play-white">
        <?php get_svg('icon-play') ?>
      </a>
    </div>
    <div class="info-container">
      <a href="<?= $post_url ?>" class="arrow-play-red">
        <?php get_svg('icon-play') ?>
      </a>
      <h2 class="title-post-card">
        <?= $title ?>
      </h2>
      <h3 class="excerpt-post-card">
        <?= $excerpt ?>
      </h3>
    </div>
    <a href="<?= $post_url ?>" class="overflow"></a>
  </div>
  <?php
}